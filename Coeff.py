import numpy as np

t0 = 0
tf = 100.
N = 10000.
dt = (tf-t0)/(N)


t = np.linspace(t0,tf,N)

K=10000

gk= 36.
gNa= 120.
gL= 0.3
Ek=-77.
ENa=55.
EL=-54.4
Cm=1.

I=20

def f1(v1,m1,h1,n1):
        I_k=gk*pow(n1,4)*(v1-Ek)
        I_Na=gNa*pow(m1,3)*h1*(v1-ENa)
        I_L=gL*(v1-EL)
        return 1/Cm*(-I_k-I_Na-I_L+I)
def f2(v1,m1,h1,n1):
        alpha_m=-0.1*(v1+40.)/(np.exp(-0.1*(v1+40.))-1.)
        betta_m=4.*np.exp(-(v1+65.)/18.)
        return alpha_m*(1.-m1)-betta_m*m1

